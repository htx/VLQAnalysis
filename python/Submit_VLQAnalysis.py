import os
import string
import time, getpass
import socket
import sys
import datetime
from VLQ_Samples import *

sys.path.append( os.getenv("ROOTCOREBIN") + "python/IFAETopFramework/" )
from BatchTools import *
from Job import *
from Samples import *

##________________________________________________________
## OPTIONS
nFilesSplit = 20 #number of files to merge in a single run of the code (chaining them)
nMerge = 10 #number of merged running of the code in a single job
channels = ["./.",]
#Pile-up
usePileUpWeight = True
#Samples
param_useSlices = True
#Top corrections
param_scaleHtSlices = False
param_applyTtbbCorrection = True
param_recomputeTTBB = False
param_computeTTCC = False
param_reweightTTcomponents = True
param_applyNNLOWeight = True
param_applyVjetsSherpa22RW = True
#Job parameters
btagOP = "77"
param_recomputeBtagSF = False
doBlind = False
dumpHistos = True
dumpTree = False
splitVLQDecays = True
doTruthAnalysis = False
param_applyMetMtwCuts = False
param_invertMetMtwCuts = False
param_useLeptonSF = True
param_drawOtherVariables = False #Set it to true with great care !!! (default should be false)
param_do1lepAna = True
param_do0lepAna = True
param_doLowBRegions = True
param_useLeptonTrigger = True
param_useMETTrigger = True
param_applyDPhiCut = True
param_invertDPhiCut = False
param_jetPtCut = "25"
param_RCJetPtCut = "200"
param_RCNSJCut = "2"
#TRF
param_doTRF = False
param_doTRF_forSmallBackgrounds = False
param_doTRF_forTtbarSysts = False
param_recomputeTRF = False
#Systematics
param_useWeightSyst = False
param_useObjectSyst = False
param_onlyDumpSystHistos = False
if False: #activate for systematics (False = no systematics applied)
    param_useWeightSyst = True
    param_useObjectSyst = True
    param_onlyDumpSystHistos = True
#Batch jobs
param_produceTarBall = True
param_diffFiles = False #object systematics in different files (True) or different trees (False)
param_queue = "at3_short"
##........................................................

##________________________________________________________
## Defines some useful variables
platform = socket.gethostname()
now = datetime.datetime.now().strftime("%Y_%m_%d_%H%M")
here = os.getcwd()
##........................................................

##________________________________________________________
## Defining the paths and the tarball
inputDir="/nfs/at3/scratch2/lvalery/VLQFiles/MBJ-2.4.24-1-0/" #place where to find the input files
sampleDat="samples_info_MBJ-2.4.24-1-0.dat"
outputDir = "/nfs/at3/scratch2/"+os.getenv("USER")+"/VLQAnalysisRun2/VLQAnalysisOutputs_" #output repository
if usePileUpWeight:
    outputDir += "PURW_"
if param_applyMetMtwCuts:
    if param_invertMetMtwCuts:
        outputDir += "METMTLOW_"
    else:
        outputDir += "METMT_"
if not param_applyDPhiCut:
    outputDir += "NODPHI_"
elif param_invertDPhiCut:
    outputDir += "LOWDPHI_"
if param_applyTtbbCorrection:
    outputDir += "ttbbCorrection_"
if param_applyNNLOWeight:
    outputDir += "NNLORw_"
if param_applyVjetsSherpa22RW:
    outputDir += "VJSherpaRW_"
if param_useObjectSyst:
    outputDir += "objSyst_"
if param_useWeightSyst:
    outputDir += "wgtSyst_"
if param_doTRF:
    outputDir += "TRF_"
if param_useLeptonSF:
    outputDir += "LepSF_"
if doBlind:
    outputDir += "BlindingApplied_"
if param_doLowBRegions:
    outputDir += "LowBRegions_"
outputDir += "jetPtCut_"+param_jetPtCut+"_"
outputDir += "btagOP"+btagOP
outputDir += "_ttbarObjects_" + now
listFolder = outputDir + "/Lists_Analysis_" + outputDir.split("/")[len(outputDir.split("/"))-1] #name of the folder containing the file lists
scriptFolder = outputDir + "/Scripts_Analysis_" + outputDir.split("/")[len(outputDir.split("/"))-1] #name of the folder containing the scripts
##........................................................

##________________________________________________________
## Creating usefull repositories
os.system("mkdir -p " + outputDir) #output files folder
os.system("mkdir -p " + listFolder) #list files folder
os.system("mkdir -p " + scriptFolder) #script files folder
##........................................................

##________________________________________________________
## Creating tarball
tarballPath=""
if param_produceTarBall:
    tarballPath = outputDir + "/AnaCode_forBatch.tgz"
    if not os.path.exists(tarballPath):
        prepareTarBall(here+"/../../",tarballPath)
##........................................................

##________________________________________________________
## Getting all samples and their associated weight/object systematics
##
Samples = []
Samples += GetDataSamples( sampleName = "QCD" )
Samples += GetDataSamples()
Samples += GetTtbarSamples ( useWeightSyst = param_useWeightSyst, useObjectSyst = param_useObjectSyst, ttbarSystSamples = False, hfSplitted = True, useHTSlices = param_useSlices )
Samples += GetOtherSamples ( useWeightSyst = param_useWeightSyst, useObjectSyst = param_useObjectSyst, includeSignals=False  )
printGoodNews("--> All samples recovered")
##........................................................

##________________________________________________________
## Loop over channels
printGoodNews("--> Performing the loop over samples and jobs")
nJobs = 0
for channel in channels:

    ##________________________________________________________
    ## Loop over samples
    for sample in Samples:

        SName = sample['name'] # sample name
        SType = sample['sampleType'] # sample type (first argument of the getSamplesUncertainties())

        iDir = inputDir
        excluded = []
        joblist = getSampleJobs(sample,InputDir=iDir+"/"+channel+"/",NFiles=nFilesSplit,UseList=False,ListFolder=listFolder,exclusions=excluded,useDiffFilesForObjSyst=param_diffFiles)
        if(not joblist):
            continue

        #Setting caracteristics for the JobSet object
        JOSet = JobSet(platform)
        JOSet.setScriptDir(scriptFolder)
        JOSet.setLogDir(outputDir)
        JOSet.setTarBall(tarballPath)#tarball sent to batch (contains all executables)
        JOSet.setJobRecoveryFile(scriptFolder+"/JobCheck.chk")
        JOSet.setQueue(param_queue)

        ##________________________________________________________
        ## Loop over jobs for this sample (multiple files or systematics)
        for iJob in range(len(joblist)):

            ## Declare the Job object (one job = one code running once)
            jO = Job(platform)

            ## Name of the executable you want to run
            jO.setExecutable("VLQAnalysis")
            jO.setDebug(False)

            name  = SType
            name += "_" + SName
            name += "_"+joblist[iJob]['objSyst']+"_"+`iJob` #name of the job

            jO.setName(name)

            # Settings of the jobs (inputs, outputs, ...)
            OFileName = "out"+jO.execName+"_"+name+".root"
            jO.addOption("outputFile",OFileName) #name of the output file
            jO.addOption("inputFile",joblist[iJob]['filelist']) #name of the input file (already got from ls)
            jO.addOption("textFileList","false")
            jO.addOption("sampleName",sample['sampleType'])
            jO.addOption("sampleDAT",sampleDat)

            #Lepton scale factors
            if param_useLeptonSF:
                jO.addOption("useLeptonsSF","true")
            else:
                jO.addOption("useLeptonsSF","false")

            #ttbb reweighting
            if param_recomputeTTBB and SType.find("ttbar")>-1:
                jO.addOption("RECOMPUTETTBBRW","true")
            else:
                jO.addOption("RECOMPUTETTBBRW","false")

            if param_computeTTCC and SType.find("ttbar")>-1:
                jO.addOption("COMPUTETTCCNLO","true")
            else:
                jO.addOption("COMPUTETTCCNLO","false")

            #ttbar fractions reweighting
            if param_reweightTTcomponents and SType.find("ttbar")>-1 and SName.find("410000")==-1:
                jO.addOption("RWTTFRACTIONS","true")
            else:
                jO.addOption("RWTTFRACTIONS","false")

            # Filter type
            filterType = "NOFILTER"
            if param_useSlices:
                filterType = "APPLYFILTER"
            jO.addOption("filterType",filterType)

            # Only systematic histograms
            if param_onlyDumpSystHistos:
                jO.addOption("onlyDumpSystHistograms","true")

            # Do TRF
            applyTRF = False
            if param_doTRF:
                applyTRF = True
            if param_doTRF_forSmallBackgrounds:
                if SType.find("W+jets")>-1 or SType.find("Z+jets")>-1 or SType.find("Singletop")>-1 or SType.find("Dibosons")>-1 or SType.find("topEW")>-1 or SType.find("Dijets")>-1:
                    applyTRF = True
            if param_doTRF_forTtbarSysts:
                if SName.find("410001")>-1 or SName.find("410002")>-1 or SName.find("410003")>-1 or SName.find("410004")>-1:
                    applyTRF = True

            if applyTRF:
                jO.addOption("doTRF","true")
            else:
                jO.addOption("doTRF","false")

            if param_recomputeTRF:
                jO.addOption("recomputeTRF","true")
            else:
                jO.addOption("recomputeTRF","false")

            #Draw other variables with the systematics ... (WANRING: leads to huge files !!!!)
            if param_drawOtherVariables:
                jO.addOption("otherVariables","true")
            else:
                jO.addOption("otherVariables","false")

            #b-tag OP
            jO.addOption("btagOP",btagOP)

            #Recompute b-tag SF
            if param_recomputeBtagSF:
                jO.addOption("RECOMPUTEBTAGSF","true")

            #Blind Option
            if doBlind:
                jO.addOption("doBlind","true")
            else:
                jO.addOption("doBlind","false")

            if SType.find("ttbar")>-1 :
                #ttbb correction
                if param_applyTtbbCorrection:
                    jO.addOption("applyTtbbCorrection","true")
                else:
                    jO.addOption("applyTtbbCorrection","false")

                #top pT weight
                if param_applyNNLOWeight and SType.find("ttbarbb")<=-1 :
                    jO.addOption("APPLYTTBARNNLOCORRECTION","true")
                else:
                    jO.addOption("APPLYTTBARNNLOCORRECTION","false")

                if param_scaleHtSlices:
                    jO.addOption("SCALETTBARHTSLICES","true")
                else:
                    jO.addOption("SCALETTBARHTSLICES","false")

            if SType.find("W+jets22")>-1 or SType.find("Z+jets22")>-1 :

                if param_applyVjetsSherpa22RW:
                    jO.addOption("APPLYVJETSSHERPA22RW","true")
                else:
                    jO.addOption("APPLYVJETSSHERPA22RW","false")


            # Weight systematics
            if param_useWeightSyst and joblist[iJob]['objSyst'].upper()=="NOMINAL_LOOSE" and SType.upper().find("DATA")==-1:
                jO.addOption("computeWeightSys","true")
            else:
                jO.addOption("computeWeightSys","false")

            # Pile-up reweighting
            if(usePileUpWeight):
                jO.addOption("usePUWeight","true")
            else:
                jO.addOption("usePUWeight","false")

            # Splitting VLQ decay types
            if(splitVLQDecays):
                jO.addOption("splitVLQDecays","true")
            else:
                jO.addOption("splitVLQDecays","false")

            # Dump histos
            if(dumpHistos):
                jO.addOption("dumpHistos","true")
            else:
                jO.addOption("dumpHistos","false")

            # Dump tree
            if(dumpTree):
                jO.addOption("dumpTree","true")
            else:
                jO.addOption("dumpTree","false")

            #Apply MET and MTW cuts (reject/enhance QCD)
            if(param_applyMetMtwCuts):
                jO.addOption("applyMetMtwCuts","true")
            else:
                jO.addOption("applyMetMtwCuts","false")

            #Invert MET and MTW cuts (enhance QCD - applyMetMtwCuts)
            if(param_invertMetMtwCuts):
                jO.addOption("invertMetMtwCuts","true")
            else:
                jO.addOption("invertMetMtwCuts","false")

            # Truth analysis
            if(doTruthAnalysis and SType.upper().find("DATA")==-1):
                jO.addOption("doTruthAnalysis","true")
            else:
                jO.addOption("doTruthAnalysis","false")

            # Add large-R jets in the outputs
            jO.addOption("uselargeRjets","false")

            # Do 1 lepton analysis
            if param_do1lepAna:
                jO.addOption("doOneLeptonAna","true")
            else:
                jO.addOption("doOneLeptonAna","false")

            # Do 0 lepton analysis
            if param_do0lepAna:
                jO.addOption("doZeroLeptonAna","true")
            else:
                jO.addOption("doZeroLeptonAna","false")

            # Add low-b multiplicity regions
            if param_doLowBRegions:
                jO.addOption("doLowBRegions","true")
            else:
                jO.addOption("doLowBRegions","false")

            # Use lepton trigger
            if param_useLeptonTrigger:
                jO.addOption("useLeptonTrigger","true")
            else:
                jO.addOption("useLeptonTrigger","false")

            # Use MET trigger
            if param_useMETTrigger:
                jO.addOption("useMETTrigger","true")
            else:
                jO.addOption("useMETTrigger","false")

            # Jet pT cut
            jO.addOption("jetPtCut",param_jetPtCut)

            # RC Jet pT cut
            jO.addOption("RCJetPtCut",param_RCJetPtCut)

            #RC Jet subjet multiplicity
            jO.addOption("RCNSUBJETSCUT", param_RCNSJCut)

            # Use deltaPhi cut
            if param_applyDPhiCut:
                jO.addOption("applydeltaphicut","true")
            else:
                jO.addOption("applydeltaphicut","false")

            if param_invertDPhiCut:
                jO.addOption("invertdeltaphicut","true")
            else:
                jO.addOption("invertdeltaphicut","false")

            # Adding the weight configuration file
            jO.addOption("WEIGHTCONFIGS","$ROOTCOREBIN/data/VLQAnalysis/list_weights.list")

            jO.setOutDir(outputDir)
            jO.addOption("sampleID",SName)

            # Input tree name (might change for systematics)
            if not param_diffFiles:
                jO.addOption("inputTree",joblist[iJob]['objectTree'])
            else:
                jO.addOption("inputTree","nominal")

            # isData flag
            if(sample['sampleType'].upper().find("DATA")>-1):
                jO.addOption("isData","true")
            else:
                jO.addOption("isData","false")

            ## SKIPPING ALREADY PROCESSED FILES
            if(os.path.exists(outputDir+"/"+OFileName)):
                printWarning("=> Already processed: skipping")
                continue

            JOSet.addJob(jO)

            if ( JOSet.size()==nMerge or joblist[iJob]['objSyst'].upper()=="NOMINAL" ):
                JOSet.writeScript()
                JOSet.submitSet()
                JOSet.clear()

        if(JOSet.size()>0):
            JOSet.writeScript()
            JOSet.submitSet()
            JOSet.clear()
##........................................................
