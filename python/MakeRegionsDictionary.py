import re

SR=['c1lep3_5jwin1bex1fjin0LTex0Hex1Vin',
'c1lep3_5jwin1bex1fjin0Tex1Lin0Hex1Vin',
'c1lep3_5jwin2bex1fjin0LTex0Hex1Vin',
'c1lep3_5jwin2bex1fjin0Tex1Lin0Hex1Vin',
'c1lep3_5jwin3bex1fjin0LTex1Hin0Vex',
'c1lep3_5jwin3bex1fjin0Tex1Lin1Hin0Vex',
'c1lep3_5jwin3bex1fjin1Tin0Lex1Hin0Vex',
'c1lep3_5jwin4bin1fjin0LTex1Hin0Vex',
'c1lep3_5jwin4bin1fjin0Tex1Lin1Hin0Vex',
'c1lep3_5jwin4bin1fjin1Tin0Lex1Hin0Vex',
'c1lep6jin1bex1fjin1Lex0Tex0Hex1Vin',
'c1lep6jin1bex1fjin0Lex1Tex0Hex1Vin',
'c1lep6jin1bex1fjin2LTin0Hex1Vin',
'c1lep6jin2bex1fjin1Lex0Tex0Hex1Vin',
'c1lep6jin2bex1fjin0Lex1Tex0Hex1Vin',
'c1lep6jin2bex1fjin2LTin0Hex1Vin',
'c1lep6jin3bex1fjin1Lex0VTex1Hin',
'c1lep6jin3bex1fjin0Lex1VTex1Hin',
'c1lep6jin3bex1fjin2VLTin1Hin',
'c1lep6jin4bin1fjin1Lex0VTex1Hin',
'c1lep6jin4bin1fjin0Lex1VTex1Hin',
'c1lep6jin4bin1fjin2VLTin1Hin']

VR=['c1lep3_5jwin1bex0fjex0Tex0Lex0Hex1Vin',
'c1lep3_5jwin1bex0fjex0Tex1Lin0Hex1Vin',
'c1lep3_5jwin1bex1fjin1LTin0Hex0Vex',
'c1lep3_5jwin1bex1fjin1Tin0Lex0Hex1Vin',
'c1lep3_5jwin2bex0fjex0Tex0Lex0Hex1Vin',
'c1lep3_5jwin2bex0fjex0Tex1Lin0Hex1Vin',
'c1lep3_5jwin2bex1fjin1LTin0Hex0Vex',
'c1lep3_5jwin2bex1fjin1Tin0Lex0Hex1Vin',
'c1lep3_5jwin3bex0fjex0Tex0Lex1Hin0Vex',
'c1lep3_5jwin3bex0fjex0Tex1Lin1Hin0Vex',
'c1lep3_5jwin3bex1fjin1VLTin0Hex',
'c1lep3_5jwin4bin0fjex0Tex0Lex1Hin0Vex',
'c1lep3_5jwin4bin0fjex0Tex1Lin1Hin0Vex',
'c1lep3_5jwin4bin1fjin1VLTin0Hex',
'c1lep6jin1bex0fjex1LTex0Hex1Vin',
'c1lep6jin1bex0fjex2LTin0Hex1Vin',
'c1lep6jin1bex1fjin0Tex0Lex1Hin1Vin',
'c1lep6jin1bex1fjin2LTin1Hin0Vex',
'c1lep6jin2bex0fjex1LTex0Hex1Vin',
'c1lep6jin2bex0fjex2LTin0Hex1Vin',
'c1lep6jin2bex1fjin0Tex0Lex1Hin1Vin',
'c1lep6jin2bex1fjin2LTin1Hin0Vex',
'c1lep6jin3bex0fjex1VLTex1Hin',
'c1lep6jin3bex0fjex2VLTin1Hin',
'c1lep6jin3bex1fjin1VLTex0Hex',
'c1lep6jin3bex1fjin2VLTin0Hex',
'c1lep6jin4bin0fjex1VLTex1Hin',
'c1lep6jin4bin1fjin1VLTex0Hex',
'c1lep6jin4bin1fjin2VLTin0Hex']

def printregions(l,typ):
  for region in l:
    legend = ""
    items = 0
    if region == "call":
      legend = "No selection"
  
    if '3_5jwin' in region:
      legend+='LJ'
      items+=1
    if '6jin' in region:
      legend+='HJ'
      items+=1

    if 'b' in region:
      legend+=', '+region[region.index('b')-1]+'b'
      items+=1
    if 'fj' in region:
      legend+=', '+region[region.index('fj')-1]+'fj'
      items+=1

    if 'TH' in region:
      legend+=', '+region[region.index('TH')-1]+'h+t_{h}'
      items+=1
    elif 'T' in region and region[region.index('T')-1].isdigit():
      legend+=', '+region[region.index('T')-1]+'t_{h}'
      items+=1

    if 'LT' in region and region[region.index('LT')-1].isdigit():
      legend+=', '+region[region.index('LT')-1]+'(t_{h}+t_{l})'
      items+=2
    elif 'L' in region and region[region.index('L')-1].isdigit():
      legend+=', '+region[region.index('L')-1]+'t_{l}'
      items+=1  

    if 'H' in region:
      legend+=', '+region[region.index('H')-1]+'h'
      items+=1
  
    if 'VLT' in region:
      legend+=', '+region[region.index('VLT')-1]+'(v+t_{l}+t_{h})'
      items+=2
    if 'VT' in region:
      legend+=', '+region[region.index('VT')-1]+'(v+t_{h})'
      items+=2

    if 'V' in region and region[region.index('V')-1].isdigit() and not 'v' in legend:
      legend+=', '+region[region.index('V')-1]+'V'
      items+=1 
    
    if items >= 4:
      splitindex = [i.start() for i in re.finditer(', ',legend)][int(items/2)]+1
      legend = '#splitline{%s}{%s}'%(legend[:splitindex],legend[splitindex+1:])

    # print region
    # print legend
    # print ""

    print "reg_"+region[1:]+" = {"
    print "\t'name':"+'"HTX_'+region+'",'
    # print "\t'legend': "+'"'+legend+'",'
    print "\t'legend': "+'"#scale[0.75]{'+legend+'}",'
    print "\t'binning':"+'"600,800,1000,1400,1600,1800,3000",'
    print "\t'binning_recoVLQ0_m':"+'"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",'
    print "\t'type':"+'"%s"'%typ
    print '}'

printregions(SR,"SIGNAL")
print "\n\n"
printregions(VR,"SIGNAL")