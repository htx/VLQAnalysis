#!/bin/python
import os
import sys

sys.path.append( os.getenv("ROOTCOREBIN") + "/python/IFAETopFramework/" )
from BatchTools import *
from Samples import *

##______________________________________________________________________
##
## Object systematics
##
## Nominal
CommonObjectSystematics =  []
CommonObjectSystematics += [getSystematics(name="nominal",nameUp="nominal",oneSided=True)] # the nominal is considered as a systematic variation
## Electron
CommonObjectSystematics += [getSystematics(name="EG_RESOLUTION_ALL",nameUp="EG_RESOLUTION_ALL__1up",nameDown="EG_RESOLUTION_ALL__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="EG_SCALE_ALL",nameUp="EG_SCALE_ALL__1up",nameDown="EG_SCALE_ALL__1down",oneSided=False)]
## Jets
CommonObjectSystematics += [getSystematics(name="JET_BJES_Response",nameUp="JET_BJES_Response__1up",nameDown="JET_BJES_Response__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_EffectiveNP_1",nameUp="JET_EffectiveNP_1__1up",nameDown="JET_EffectiveNP_1__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_EffectiveNP_2",nameUp="JET_EffectiveNP_2__1up",nameDown="JET_EffectiveNP_2__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_EffectiveNP_3",nameUp="JET_EffectiveNP_3__1up",nameDown="JET_EffectiveNP_3__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_EffectiveNP_4",nameUp="JET_EffectiveNP_4__1up",nameDown="JET_EffectiveNP_4__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_EffectiveNP_5",nameUp="JET_EffectiveNP_5__1up",nameDown="JET_EffectiveNP_5__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_EffectiveNP_6",nameUp="JET_EffectiveNP_6__1up",nameDown="JET_EffectiveNP_6__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_EffectiveNP_7",nameUp="JET_EffectiveNP_7__1up",nameDown="JET_EffectiveNP_7__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_EffectiveNP_8restTerm",nameUp="JET_EffectiveNP_8restTerm__1up",nameDown="JET_EffectiveNP_8restTerm__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_EtaIntercalibration_NonClosure",nameUp="JET_EtaIntercalibration_NonClosure__1up",nameDown="JET_EtaIntercalibration_NonClosure__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_EtaIntercalibration_Modelling",nameUp="JET_EtaIntercalibration_Modelling__1up",nameDown="JET_EtaIntercalibration_Modelling__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_EtaIntercalibration_TotalStat",nameUp="JET_EtaIntercalibration_TotalStat__1up",nameDown="JET_EtaIntercalibration_TotalStat__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_Flavor_Composition",nameUp="JET_Flavor_Composition__1up",nameDown="JET_Flavor_Composition__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_Flavor_Response",nameUp="JET_Flavor_Response__1up",nameDown="JET_Flavor_Response__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_Pileup_OffsetMu",nameUp="JET_Pileup_OffsetMu__1up",nameDown="JET_Pileup_OffsetMu__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_Pileup_OffsetNPV",nameUp="JET_Pileup_OffsetNPV__1up",nameDown="JET_Pileup_OffsetNPV__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_Pileup_PtTerm",nameUp="JET_Pileup_PtTerm__1up",nameDown="JET_Pileup_PtTerm__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_Pileup_RhoTopology",nameUp="JET_Pileup_RhoTopology__1up",nameDown="JET_Pileup_RhoTopology__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_PunchThrough_MC15",nameUp="JET_PunchThrough_MC15__1up",nameDown="JET_PunchThrough_MC15__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_SingleParticle_HighPt",nameUp="JET_SingleParticle_HighPt__1up",nameDown="JET_SingleParticle_HighPt__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_Rrk_Tracking_mass",nameUp="JET_Rrk_Tracking_mass__1up",nameDown="JET_Rrk_Tracking_mass__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_Rtrk_Baseline_mass",nameUp="JET_Rtrk_Baseline_mass__1up",nameDown="JET_Rtrk_Baseline_mass__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_Rtrk_Modelling_mass",nameUp="JET_Rtrk_Modelling_mass__1up",nameDown="JET_Rtrk_Modelling_mass__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_Rtrk_TotalStat_mass_mass",nameUp="JET_Rtrk_TotalStat_mass__1up",nameDown="JET_Rtrk_TotalStat_mass__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="JET_JER_SINGLE_NP__1up",nameUp="JET_JER_SINGLE_NP__1up",nameDown="",oneSided=True)]
CommonObjectSystematics += [getSystematics(name="JET_JMR__1up",nameUp="JET_JMR__1up",nameDown="",oneSided=True)]
#MET
CommonObjectSystematics += [getSystematics(name="MET_SoftTrk_ResoPara",nameUp="MET_SoftTrk_ResoPara",nameDown="",oneSided=True)]
CommonObjectSystematics += [getSystematics(name="MET_SoftTrk_ResoPerp",nameUp="MET_SoftTrk_ResoPerp",nameDown="",oneSided=True)]
CommonObjectSystematics += [getSystematics(name="MET_SoftTrk_Scale",nameUp="MET_SoftTrk_ScaleUp",nameDown="MET_SoftTrk_ScaleDown",oneSided=False)]
#Muon
CommonObjectSystematics += [getSystematics(name="MUON_ID",nameUp="MUON_ID__1up",nameDown="MUON_ID__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="MUON_MS",nameUp="MUON_MS__1up",nameDown="MUON_MS__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="MUON_SCALE",nameUp="MUON_SCALE__1up",nameDown="MUON_SCALE__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="MUON_SAGITTA_RESBIAS",nameUp="MUON_SAGITTA_RESBIAS__1up",nameDown="MUON_SAGITTA_RESBIAS__1down",oneSided=False)]
CommonObjectSystematics += [getSystematics(name="MUON_SAGITTA_RHO",nameUp="MUON_SAGITTA_RHO__1up",nameDown="MUON_SAGITTA_RHO__1down",oneSided=False)]

##______________________________________________________________________
##
def GetTtbarSamples( useWeightSyst=False, useObjectSyst=False, hfSplitted=True, ttbarSystSamples=False, useHTSlices = True ):
    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]

    Samples = []
    ttbarTypes = ["ttbarlight","ttbarbb","ttbarcc"]
    if not hfSplitted:
        ttbarTypes = ["ttbar",]

    for ttbarType in ttbarTypes:
        Samples     += [getSampleUncertainties(ttbarType,    "410470.", ObjectSystematics, WeightSystematics)]
        if useHTSlices: 
            Samples     += [getSampleUncertainties(ttbarType,    "407342.", ObjectSystematics, WeightSystematics)]
            Samples     += [getSampleUncertainties(ttbarType,    "407343.", ObjectSystematics, WeightSystematics)]
            Samples     += [getSampleUncertainties(ttbarType,    "407344.", ObjectSystematics, WeightSystematics)]
            #Samples     += [getSampleUncertainties(ttbarType,    "407012.", ObjectSystematics, WeightSystematics)]
        if ttbarSystSamples:#to be updated
            Samples     += [getSampleUncertainties(ttbarType+"radHi",      "410001.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
            Samples     += [getSampleUncertainties(ttbarType+"radLow",     "410002.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
            Samples     += [getSampleUncertainties(ttbarType+"aMCHer",     "410003.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
            Samples     += [getSampleUncertainties(ttbarType+"PowHer",     "410004.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
            if useHTSlices:
                Samples     += [getSampleUncertainties(ttbarType+"radHi",      "407029.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"radHi",      "407030.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"radHi",      "407031.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"radHi",      "407032.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"radLow",     "407033.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"radLow",     "407034.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"radLow",     "407035.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"radLow",     "407036.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"PowHer",     "407037.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"PowHer",     "407038.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"PowHer",     "407039.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties(ttbarType+"PowHer",     "407040.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    return Samples

##______________________________________________________________________
##
def GetNewTTbarGenerators( useWeightSyst=False, useObjectSyst=False ):

    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]

    Samples = []
    Samples     += [getSampleUncertainties("ttbarPowP8",    "410470.",    [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties("ttbarSherpaMEPSNLOAllHad",  "410186.",    [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties("ttbarSherpaMEPSNLOLepP",    "410187.",    [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties("ttbarSherpaMEPSNLOLepM",    "410188.",    [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties("ttbarSherpaMEPSNLODilep",   "410189.",    [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties("ttbarMGP8",    "407200.",    [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties("ttbarMGP8",    "407201.",    [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties("ttbarMGP8",    "407202.",    [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties("ttbarMGP8",    "407203.",    [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties("ttbarMGP8",    "407204.",    [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    return Samples

##______________________________________________________________________
##
def GetOtherSamples ( useWeightSyst=False, useObjectSyst=False, 
                      includeVLQ=True, includeHBSM=True, include4tops=True, includeFCNC=True, includeHpluslm=True, includeSignals=True, includeSingletopSystSamples=False ):
    Samples =  []
    Samples += GetWSamplesSherpa221( useWeightSyst, useObjectSyst )
    Samples += GetZSamplesSherpa221( useWeightSyst, useObjectSyst )
    Samples += GetSingleTopSamples( useWeightSyst, useObjectSyst, SingletopSystSamples=includeSingletopSystSamples )
    Samples += GetTopEWSamples( useWeightSyst, useObjectSyst )
    Samples += GetTtHSamples( useWeightSyst, useObjectSyst )
    Samples += GetDibosonSamples( useWeightSyst, useObjectSyst )
    # Samples += Get4TopsSamples( useWeightSyst, useObjectSyst )
    if includeSignals:
        if include4tops:
            Samples += Get4topsCISamples( useWeightSyst, useObjectSyst )
            Samples += GetUEDRPPSamples( useWeightSyst, useObjectSyst )
        if includeHBSM:
            Samples += GetHBSMSamples( useWeightSyst, useObjectSyst )
        if includeFCNC:
            Samples += GetFCNCSamples( useWeightSyst, useObjectSyst )
        if includeHpluslm:
            Samples += GetHpluslmSamples (useWeightSyst, useObjectSyst)
        if includeVLQ:
            Samples += GetVLQTSamples( useWeightSyst, useObjectSyst )
    return Samples

##_____________________________________________________________________
##
def GetWSamplesSherpa221( useWeightSyst=False, useObjectSyst=False, name = "W+jets"):

    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]

    Samples     =  []
    Samples     += [getSampleUncertainties(name,"364170.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364171.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364172.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364173.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364174.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364175.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364176.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364177.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364178.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364179.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364180.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364181.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364182.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364183.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364156.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364157.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364158.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364159.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364160.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364161.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364162.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364163.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364164.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364165.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364166.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364167.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364168.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364169.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364184.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364185.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364186.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364187.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364188.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364189.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364190.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364191.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364192.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364193.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364194.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364195.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364196.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364197.", ObjectSystematics , WeightSystematics)]
    return Samples

##_____________________________________________________________________
##
def GetZSamplesSherpa221( useWeightSyst=False, useObjectSyst=False, name = "Z+jets"):

    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]

    Samples     =  []
    ######## Z+jets ########### 
    Samples     += [getSampleUncertainties(name,"364114.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364115.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364116.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364117.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364118.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364119.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364120.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364121.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364122.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364123.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364124.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364125.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364126.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364127.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364100.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364101.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364102.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364103.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364104.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364105.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364106.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364107.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364108.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364109.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364110.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364111.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364112.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364113.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364128.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364129.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364130.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364131.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364132.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364133.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364134.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364135.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364136.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364137.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364138.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364139.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364140.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364141.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364204.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364205.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364206.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364207.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364208.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364209.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364198.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364199.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364200.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364201.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364202.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364203.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364210.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364211.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364212.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364213.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364214.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364215.", ObjectSystematics , WeightSystematics)]

    return Samples


##______________________________________________________________________
##
def GetSingleTopSamples( useWeightSyst=False, useObjectSyst=False, name = "Singletop",style="simple", SingletopSystSamples=False):

    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]

    Samples     =  []
    if style == "simple":
        Samples     += [getSampleUncertainties(name,"410658.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name,"410659.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name,"410646.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name,"410647.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name,"410644.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name,"410645.", ObjectSystematics , WeightSystematics)]

    else:
        Samples     += [getSampleUncertainties(name+"tchan", "410658.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"tchan", "410659.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"Wtprod","410646.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"Wtprod","410647.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"schan", "410644.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"schan", "410645.", ObjectSystematics , WeightSystematics)]

    if SingletopSystSamples:
        #t-channel
        Samples     += [getSampleUncertainties(name+"tchanradLo","410017.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"tchanradHi","410018.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"tchanradLo","410019.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"tchanradHi","410020.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"tchanaMCaNLOHpp","410141.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"tchanPowHpp","410047.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"tchanPowHpp","410048.", ObjectSystematics , WeightSystematics)]
        #Wt-channel
        Samples     += [getSampleUncertainties(name+"WtDiagSub","410062.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"WtDiagSub","410063.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"WtprodradHi","410099.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"WtprodradLo","410100.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"WtprodradHi","410101.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"WtprodradLo","410102.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"WtprodPowHer","410147.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"WtprodPowHer","410148.", ObjectSystematics , WeightSystematics)]
        #s-channel
        Samples     += [getSampleUncertainties(name+"schanradHi","410107.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"schanradLo","410108.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"schanradHi","410109.", ObjectSystematics , WeightSystematics)]
        Samples     += [getSampleUncertainties(name+"schanradLo","410110.", ObjectSystematics , WeightSystematics)]
    return Samples

##______________________________________________________________________
##
def GetDibosonSamples( useWeightSyst=False, useObjectSyst=False, name = "Dibosons"):

    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]

    Samples     =  []
    Samples     += [getSampleUncertainties(name,"364250.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364253.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364254.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364255.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364288.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364289.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364290.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"363355.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"363356.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"363357.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"363358.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"363359.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"363360.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"363489.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"363494.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364283.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364284.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364285.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"364287.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"345705.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"345706.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"345723.", ObjectSystematics , WeightSystematics)]
    return Samples

##______________________________________________________________________
##
def GetTopEWSamples( useWeightSyst=False, useObjectSyst=False, name = "topEW"):

    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]

    Samples     =  []
    Samples     += [getSampleUncertainties(name,"410155.", ObjectSystematics , WeightSystematics)] #ttW
    Samples     += [getSampleUncertainties(name,"410156.", ObjectSystematics , WeightSystematics)] #ttZnunu
    Samples     += [getSampleUncertainties(name,"410157.", ObjectSystematics , WeightSystematics)] #ttZqq
    Samples     += [getSampleUncertainties(name,"410218.", ObjectSystematics , WeightSystematics)] #ttee
    Samples     += [getSampleUncertainties(name,"410219.", ObjectSystematics , WeightSystematics)] #ttmumu
    Samples     += [getSampleUncertainties(name,"410220.", ObjectSystematics , WeightSystematics)] #tttautau
    Samples     += [getSampleUncertainties(name,"410276.", ObjectSystematics , WeightSystematics)] #ttee_mll_1_5
    Samples     += [getSampleUncertainties(name,"410277.", ObjectSystematics , WeightSystematics)] #ttmumu_mll_1_5
    Samples     += [getSampleUncertainties(name,"410278.", ObjectSystematics , WeightSystematics)] #tttautau_mll_1_5
    return Samples

##______________________________________________________________________
##
def GetTtHSamples( useWeightSyst=False, useObjectSyst=False, name = "ttH"):

    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]

    Samples     =  []
    Samples     += [getSampleUncertainties(name,"345874.", ObjectSystematics , WeightSystematics)] #ttH125_hdamp352p5_semilep
    Samples     += [getSampleUncertainties(name,"345875.", ObjectSystematics , WeightSystematics)] #ttH125_hdamp352p5_dilep
    return Samples

##______________________________________________________________________
##
def Get4TopsSamples( useWeightSyst=False, useObjectSyst=False, name = "SM4tops"):

    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]

    Samples     =  []
    Samples     += [getSampleUncertainties(name,"410080.", ObjectSystematics , WeightSystematics)]
    return Samples

##______________________________________________________________________
##
def GetVLQSamples( useWeightSyst=False, useObjectSyst=False):
    Samples =  []
    Samples += GetVLQTSamples( useWeightSyst, useObjectSyst )
    return Samples

##______________________________________________________________________
##
def GetBenchmarkVLQSamples( useWeightSyst=False, useObjectSyst=False):

    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]

    Samples     =  []
    Samples     += [getSampleUncertainties("VLQ_TT_1400","302482.", ObjectSystematics , WeightSystematics)]#TT 1400
    Samples     += [getSampleUncertainties("ttbar","307018.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("ttbar","307061.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties("WTht2000","307018.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties("ZTht2000","307061.", ObjectSystematics , WeightSystematics)]
    return Samples

##______________________________________________________________________
##
def GetVLQTSamples( useWeightSyst=False, useObjectSyst=False):

    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]

    Samples     =  []
    Samples     += [getSampleUncertainties("VLQ_TT_600","302469.",  ObjectSystematics , WeightSystematics)]#TT 600
    Samples     += [getSampleUncertainties("VLQ_TT_700","302470.",  ObjectSystematics , WeightSystematics)]#TT 700
    Samples     += [getSampleUncertainties("VLQ_TT_750","302471.",  ObjectSystematics , WeightSystematics)]#TT 750
    Samples     += [getSampleUncertainties("VLQ_TT_800","302472.",  ObjectSystematics , WeightSystematics)]#TT 800
    Samples     += [getSampleUncertainties("VLQ_TT_850","302473.",  ObjectSystematics , WeightSystematics)]#TT 850
    Samples     += [getSampleUncertainties("VLQ_TT_900","302474.",  ObjectSystematics , WeightSystematics)]#TT 900
    Samples     += [getSampleUncertainties("VLQ_TT_950","302475.",  ObjectSystematics , WeightSystematics)]#TT 950
    Samples     += [getSampleUncertainties("VLQ_TT_1000","302476.", ObjectSystematics , WeightSystematics)]#TT 1000
    Samples     += [getSampleUncertainties("VLQ_TT_1050","302477.", ObjectSystematics , WeightSystematics)]#TT 1050
    Samples     += [getSampleUncertainties("VLQ_TT_1100","302478.", ObjectSystematics , WeightSystematics)]#TT 1100
    Samples     += [getSampleUncertainties("VLQ_TT_1150","302479.", ObjectSystematics , WeightSystematics)]#TT 1150
    Samples     += [getSampleUncertainties("VLQ_TT_1200","302480.", ObjectSystematics , WeightSystematics)]#TT 1200
    Samples     += [getSampleUncertainties("VLQ_TT_1300","302481.", ObjectSystematics , WeightSystematics)]#TT 1300
    Samples     += [getSampleUncertainties("VLQ_TT_1400","302482.", ObjectSystematics , WeightSystematics)]#TT 1400
    Samples     += [getSampleUncertainties("VLQ_TT_1500","308294.", ObjectSystematics , WeightSystematics)]#TT 1500                                                                  
    Samples     += [getSampleUncertainties("VLQ_TT_1600","308295.", ObjectSystematics , WeightSystematics)]#TT 1600 

    return Samples

##______________________________________________________________________
##
def GetSingleVLQSamples( useWeightSyst=False, useObjectSyst=False):

    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]
        
    Samples     =  []

    Samples     += [getSampleUncertainties("sVLQ_WTHt11K03","310778.", ObjectSystematics , WeightSystematics)]
    # Samples     += [getSampleUncertainties("sVLQ_WTHt16K03","311376.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("sVLQ_WTHt16K05","311377.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("sVLQ_WTHt20K05","310777.", ObjectSystematics , WeightSystematics)]
    # Samples     += [getSampleUncertainties("sVLQ_WTHt20K05R20","307018.", ObjectSystematics , WeightSystematics)]

    Samples     += [getSampleUncertainties("sVLQ_WTZt11K03","310776.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("sVLQ_WTZt16K05","306997.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("sVLQ_WTZt20K05","310779.", ObjectSystematics , WeightSystematics)]

    Samples     += [getSampleUncertainties("sVLQ_ZTHt11K05","307054.", ObjectSystematics , WeightSystematics)]
    # Samples     += [getSampleUncertainties("sVLQ_ZTHt16K03","311365.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("sVLQ_ZTHt16K05","307059.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("sVLQ_ZTHt20K05","307061.", ObjectSystematics , WeightSystematics)]

    Samples     += [getSampleUncertainties("sVLQ_ZTZt11K05","307042.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("sVLQ_ZTZt16K05","307047.", ObjectSystematics , WeightSystematics)]

    return Samples

def GetVLQBSamples( useWeight=False, useObjectSys=False):

    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]

    Samples     =  []
    Samples     += [getSampleUncertainties("VLQ_BB_600","302487.",  ObjectSystematics , WeightSystematics)]#BB 600
    Samples     += [getSampleUncertainties("VLQ_BB_700","302488.",  ObjectSystematics , WeightSystematics)]#BB 700
    Samples     += [getSampleUncertainties("VLQ_BB_750","302489.",  ObjectSystematics , WeightSystematics)]#BB 750
    Samples     += [getSampleUncertainties("VLQ_BB_800","302490.",  ObjectSystematics , WeightSystematics)]#BB 800
    Samples     += [getSampleUncertainties("VLQ_BB_850","302491.",  ObjectSystematics , WeightSystematics)]#BB 850
    Samples     += [getSampleUncertainties("VLQ_BB_900","302492.",  ObjectSystematics , WeightSystematics)]#BB 900
    Samples     += [getSampleUncertainties("VLQ_BB_950","302493.",  ObjectSystematics , WeightSystematics)]#BB 950
    Samples     += [getSampleUncertainties("VLQ_BB_1000","302494.", ObjectSystematics , WeightSystematics)]#BB 1000
    Samples     += [getSampleUncertainties("VLQ_BB_1050","302495.", ObjectSystematics , WeightSystematics)]#BB 1050
    Samples     += [getSampleUncertainties("VLQ_BB_1100","302496.", ObjectSystematics , WeightSystematics)]#BB 1100
    Samples     += [getSampleUncertainties("VLQ_BB_1150","302497.", ObjectSystematics , WeightSystematics)]#BB 1150
    Samples     += [getSampleUncertainties("VLQ_BB_1200","302498.", ObjectSystematics , WeightSystematics)]#BB 1200
    Samples     += [getSampleUncertainties("VLQ_BB_1300","302499.", ObjectSystematics , WeightSystematics)]#BB 1300
    Samples     += [getSampleUncertainties("VLQ_BB_1400","302500.", ObjectSystematics , WeightSystematics)]#BB 1400
    Samples     += [getSampleUncertainties("VLQ_BB_1500","308303.", ObjectSystematics , WeightSystematics)]#BB 1500
    Samples     += [getSampleUncertainties("VLQ_BB_1600","308304.", ObjectSystematics , WeightSystematics)]#BB 1600

##______________________________________________________________________
##
def GetUEDRPPSamples( useWeightSyst=False, useObjectSyst=False ):

    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]

    Samples     =  []
    Samples     += [getSampleUncertainties("UEDRPP_1000","302055.", ObjectSystematics , WeightSystematics)]#UED RPP 1 TeV
    Samples     += [getSampleUncertainties("UEDRPP_1200","302056.", ObjectSystematics , WeightSystematics)]#UED RPP 1.2 TeV
    Samples     += [getSampleUncertainties("UEDRPP_1400","302057.", ObjectSystematics , WeightSystematics)]#UED RPP 1.4 TeV
    Samples     += [getSampleUncertainties("UEDRPP_1600","302058.", ObjectSystematics , WeightSystematics)]#UED RPP 1.6 TeV
    Samples     += [getSampleUncertainties("UEDRPP_1800","302059.", ObjectSystematics , WeightSystematics)]#UED RPP 1.8 TeV
    return Samples

##______________________________________________________________________
##
def GetHBSMSamples( useWeightSyst=False, useObjectSyst=False ):

    ObjectSystematics = []
    WeightSystematics = []
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]

    Samples     =  []
    Samples     += [getSampleUncertainties("bbtt_400","344066.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("bbtt_500","344067.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("bbtt_600","344068.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("bbtt_700","344069.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("bbtt_800","344071.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("bbtt_900","344072.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("bbtt_1000","344073.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("ttbb_200","344896.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("ttbb_300","344897.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("ttbb_400","344898.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("ttbb_500","344899.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("ttbb_600","344900.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("ttbb_700","344901.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("ttbb_800","344902.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("ttbb_900","344903.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("ttbb_1000","344904.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("tttt_400","344074.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("tttt_500","344075.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("tttt_600","344076.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("tttt_700","344077.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("tttt_800","344079.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("tttt_900","344080.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("tttt_1000","344081.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hplus_200","341541.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hplus_250","341543.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hplus_300","341545.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hplus_350","341546.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hplus_400","341547.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hplus_500","341548.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hplus_600","341549.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hplus_700","341550.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hplus_800","341551.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hplus_900","341552.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hplus_1000","341553.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hplus_1200","341554.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hplus_1400","341555.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hplus_1600","341556.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hplus_1800","341557.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Hplus_2000","341558.", ObjectSystematics , WeightSystematics)]
    return Samples

##______________________________________________________________________
##
def Get4topsCISamples( useWeightSyst=False, useObjectSyst=False ):
    ObjectSystematics = []
    WeightSystematics = []

    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]

    Samples     =  []
    Samples     += [getSampleUncertainties("CI4tops","302777.", ObjectSystematics , WeightSystematics)]#Contact interaction
    return Samples

##______________________________________________________________________
##
def GetDataSamples( sampleName = "Data", data_type = "TOPQ4" ):
    Samples     =  []
    Samples     += [getSampleUncertainties(sampleName+".grp15","AllYear.data_2015"+".DAOD_"+data_type,
                                           [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    Samples     += [getSampleUncertainties(sampleName+".grp16","AllYear.data_2016"+".DAOD_"+data_type,
                                           [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    Samples     += [getSampleUncertainties(sampleName+".grp17","AllYear.data_2017"+".DAOD_"+data_type,
                                           [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    Samples     += [getSampleUncertainties(sampleName+".grp18","phys-higgs.data_2018",
                                           [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]


    #Samples     += [getSampleUncertainties(sampleName,"AllYear.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    #Samples     += [getSampleUncertainties(sampleName,"data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName,"periodF.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName,"periodA.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName,"periodC.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName,"periodD.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName,"periodE.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName,"periodG.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName,"periodI.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName,"periodK.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName,"periodL.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName,"periodB.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName,"AllYear.data_2015.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]

    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodA_21","periodA.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodB_21","periodB.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodC_21","periodC.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodD_21","periodD.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodE_21","periodE.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodF_21","periodF.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodG_21","periodG.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodI_21","periodI.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodK_21","periodK.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodL_21","periodL.data_2016.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]

    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodA_20.7","periodA.data.DAOD_"+data_type,[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodB_20.7","periodB.data.DAOD_"+data_type,[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodC_20.7","periodC.data.DAOD_"+data_type,[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodD_20.7","periodD.data.DAOD_"+data_type,[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodE_20.7","periodE.data.DAOD_"+data_type,[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodF_20.7","periodF.data.DAOD_"+data_type,[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodG_20.7","periodG.data.DAOD_"+data_type,[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodI_20.7","periodI.data.DAOD_"+data_type,[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodK_20.7","periodK.data.DAOD_"+data_type,[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    # Samples     += [getSampleUncertainties(sampleName+"_2016_periodL_20.7","periodL.data.DAOD_"+data_type,[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    return Samples;

##______________________________________________________________________
##
def GetQCDSamples( data_type = "TOPQ1" ):
    Samples     =  []
    Samples     += [getSampleUncertainties("QCDE","QCD.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    Samples     += [getSampleUncertainties("QCDMU","QCD.DAOD_"+data_type+".",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    return Samples;



##______________________________________________________________________
##
def GetSUSYSamples( useWeightSyst=False, useObjectSyst=False ):
    ObjectSystematics = []
    WeightSystematics = []

    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]

    Samples     =  []
    Samples     += [getSampleUncertainties("Gtt_900_1", "370100.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_900_200", "370101.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_900_400", "370102.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_900_545", "370103.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1000_1", "370104.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1000_200", "370105.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1000_400", "370106.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1000_600", "370107.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1000_645", "370108.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1100_1", "370109.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1100_200", "370110.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1100_400", "370111.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1100_600", "370112.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1100_745", "370113.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1200_1", "370114.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1200_100", "370115.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1200_200", "370116.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1200_400", "370117.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1200_600", "370118.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1200_700", "370119.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1200_800", "370120.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1200_845", "370121.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1200_855", "370122.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1300_1", "370123.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1300_200", "370124.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1300_400", "370125.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1300_600", "370126.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1300_800", "370127.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1300_945", "370128.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1400_1", "370129.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1400_100", "370130.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1400_200", "370131.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1400_400", "370132.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1400_600", "370133.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1400_800", "370134.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1400_1000", "370135.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1400_1045", "370136.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1500_1", "370137.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1500_200", "370138.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1500_400", "370139.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1500_600", "370140.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1500_800", "370141.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1500_1000", "370142.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1500_1145", "370143.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1600_1", "370144.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1600_200", "370145.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1600_400", "370146.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1600_600", "370147.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1600_800", "370148.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1600_1000", "370149.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1600_1200", "370150.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1600_1245", "370151.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1700_1", "370152.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1700_200", "370153.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1700_400", "370154.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1700_600", "370155.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1700_800", "370156.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1700_1000", "370157.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1700_1200", "370158.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1700_1345", "370159.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1800_1", "370160.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1800_200", "370161.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1800_400", "370162.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1800_600", "370163.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1800_800", "370164.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1800_1000", "370165.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1800_1200", "370166.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1800_1400", "370167.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1800_1445", "370168.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1900_1", "370169.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1900_200", "370170.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1900_400", "370171.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1900_600", "370172.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1900_800", "370173.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1900_1000", "370174.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1900_1200", "370175.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1900_1400", "370176.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_1900_1545", "370177.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2000_1", "370178.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2000_200", "370179.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2000_400", "370180.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2000_600", "370181.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2000_800", "370182.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2000_1000", "370183.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2000_1200", "370184.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2000_1400", "370185.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2000_1600", "370186.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2000_1645", "370187.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2100_1", "370239.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2100_200", "370240.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2100_400", "370241.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2100_800", "370243.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2100_1000", "370244.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2100_1200", "370245.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2100_1400", "370246.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2100_1600", "370247.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2100_1745", "370248.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2200_1", "370249.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2200_200", "373420.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2200_400", "373421.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2200_600", "373422.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2200_800", "373423.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2200_1000", "373424.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2200_1200", "373425.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2200_1400", "373426.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2200_1600", "373427.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2200_1800", "373428.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2300_1", "373429.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2300_200", "373430.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2300_400", "373431.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2300_600", "373432.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2300_800", "373433.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2300_1000", "373434.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2300_1200", "373435.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2300_1400", "373436.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2300_1600", "373437.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2300_1800", "373438.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2400_1", "373439.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2400_200", "373440.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2400_400", "373441.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2400_600", "373442.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2400_800", "373443.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2400_1000", "373444.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2400_1200", "373445.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2400_1400", "373446.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2400_1600", "373447.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties("Gtt_2400_2000", "373448.", ObjectSystematics , WeightSystematics)]
    return Samples;
