#!/bin/python
#
# Dictionnary containing all the information about the various regions to be
# considered in the analysis.
# A few lists are also built in order to avoid the manual intervention of the
# user and the duplication of information
#
#
################################################################################
#
# 1L regions
#
################################################################################
#
#
# Fit regions
#
#


reg_1lep3_5jwin1bex1fjin0Tex1Lin0Hex1Vin = {
    'name':"HTX_c1lep3_5jwin1bex1fjin0Tex1Lin0Hex1Vin",
    'legend': "#splitline{1l, 3-5j, 1b, #geq1fj}{0h, #geq1v, 0t_{h}, #geq1t_{l}}",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep3_5jwin2bex1fjin0Tex1Lin0Hex1Vin = {
    'name':"HTX_c1lep3_5jwin2bex1fjin0Tex1Lin0Hex1Vin",
    'legend': "#splitline{1l, 3-5j, 2b, #geq1fj}{0h, #geq1v, 0t_{h}, #geq1t_{l}}",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep3_5jwin1bex1fjin0LTex0Hex1Vin = {
    'name':"HTX_c1lep3_5jwin1bex1fjin0LTex0Hex1Vin",
    'legend': "#splitline{1l, 3-5j, 1b, #geq1fj}{0h, #geq1v, 0(t_{l}+t_{h})}",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep3_5jwin2bex1fjin0LTex0Hex1Vin = {
    'name':"HTX_c1lep3_5jwin2bex1fjin0LTex0Hex1Vin",
    'legend': "#splitline{1l, 3-5j, 2b, #geq1fj}{0h, #geq1v, 0(t_{l}+t_{h})}",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep3_5jwin3bex1fjin0Tex1Lin1Hin0Vex = {
    'name':"HTX_c1lep3_5jwin3bex1fjin0Tex1Lin1Hin0Vex",
    'legend': "#splitline{1l, 3-5j, 3b, #geq1fj}{#geq1h, 0v, 0t_{h}, #geq1t_{l}}",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#2000,2500,3000,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep3_5jwin4bin1fjin0Tex1Lin1Hin0Vex = {
    'name':"HTX_c1lep3_5jwin4bin1fjin0Tex1Lin1Hin0Vex",
    'legend': "#splitline{1l, 3-5j, #geq4b, #geq1fj}{#geq1h, 0v, 0t_{h}, #geq1t_{l}}",
    'binning':"600,800,1000,1400,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#2000,2500,3000,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep3_5jwin3bex1fjin0LTex1Hin0Vex = {
    'name':"HTX_c1lep3_5jwin3bex1fjin0LTex1Hin0Vex",
    'legend': "#splitline{1l, 3-5j, 3b, #geq1fj}{#geq1h, 0(t_{l}+t_{h}), 0v}",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#2000,2500,3000,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep3_5jwin4bin1fjin0LTex1Hin0Vex = {
    'name':"HTX_c1lep3_5jwin4bin1fjin0LTex1Hin0Vex",
    'legend': "#splitline{1l, 3-5j, #geq4b, #geq1fj}{#geq1h, 0(t_{l}+t_{h}), 0v}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#2000,2500,3000,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep6jin1bex1fjin1Lex0Tex0Hex1Vin = {
    'name':"HTX_c1lep6jin1bex1fjin1Lex0Tex0Hex1Vin",
    'legend': "#splitline{1l, #geq6j, 1b, #geq1fj}{0h, 1t_{l}, 0t_{h}, #geq1v}",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep6jin2bex1fjin1Lex0Tex0Hex1Vin = {
    'name':"HTX_c1lep6jin2bex1fjin1Lex0Tex0Hex1Vin",
    'legend': "#splitline{1l, #geq6j, 2b, #geq1fj}{0h, 1t_{l}, 0t_{h}, #geq1v}",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep6jin1bex1fjin0Lex1Tex0Hex1Vin = {
    'name':"HTX_c1lep6jin1bex1fjin0Lex1Tex0Hex1Vin",
    'legend': "#splitline{1l, #geq6j, 1b, #geq1fj}{0h, 0t_{l}, 1t_{h}, #geq1v}",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep6jin2bex1fjin0Lex1Tex0Hex1Vin = {
    'name':"HTX_c1lep6jin2bex1fjin0Lex1Tex0Hex1Vin",
    'legend': "#splitline{1l, #geq6j, 2b, #geq1fj}{0h, 0t_{l}, 1t_{h}, #geq1v}",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep6jin1bex1fjin2LTin0Hex1Vin = {
    'name':"HTX_c1lep6jin1bex1fjin2LTin0Hex1Vin",
    'legend': "#splitline{1l, #geq6j, 1b, #geq1fj}{0h, #geq2(t_{l}+t_{h}), #geq1v}",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep6jin2bex1fjin2LTin0Hex1Vin = {
    'name':"HTX_c1lep6jin2bex1fjin2LTin0Hex1Vin",
    'legend': "#splitline{1l, #geq6j, 2b, #geq1fj}{0h, #geq2(t_{l}+t_{h}), #geq1v}",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep6jin3bex1fjin0Lex1VTex1Hin = {
    'name':"HTX_c1lep6jin3bex1fjin0Lex1VTex1Hin",
    'legend': "#splitline{1l, #geq6j, 3b, #geq1fj}{#geq1h, 1(v+t_{h}), 0t_{l}}",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep6jin4bin1fjin0Lex1VTex1Hin = {
    'name':"HTX_c1lep6jin4bin1fjin0Lex1VTex1Hin",
    'legend': "#splitline{1l, #geq6j, #geq4b, #geq1fj}{#geq1h, 1(v+t_{h}), 0t_{l}}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep6jin3bex1fjin1Lex0VTex1Hin = {
    'name':"HTX_c1lep6jin3bex1fjin1Lex0VTex1Hin",
    'legend': "#splitline{1l, #geq6j, 3b, #geq1fj}{#geq1h, 0(v+t_{h}), 1t_{l}}",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep6jin4bin1fjin1Lex0VTex1Hin = {
    'name':"HTX_c1lep6jin4bin1fjin1Lex0VTex1Hin",
    'legend': "#splitline{1l, #geq6j, #geq4b, #geq1fj}{#geq1h, 0(v+t_{h}), 1lt_{l}}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep6jin3bex1fjin2VLTin1Hin = {
    'name':"HTX_c1lep6jin3bex1fjin2VLTin1Hin",
    'legend': "#splitline{1l, #geq6j, 3b, #geq1fj}{#geq1h, #geq2(t_{l}+t_{h}+v)}",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#,3500,4000,4500,5000",
    'type':"SIGNAL"
}
reg_1lep6jin4bin1fjin2VLTin1Hin = {
    'name':"HTX_c1lep6jin4bin1fjin2VLTin1Hin",
    'legend': "#splitline{1l, #geq6j, #geq4b, #geq1fj}{#geq1h, #geq2(t_{l}+t_{h}+v)}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",#,3500,4000,4500,5000",
    'type':"SIGNAL"
}

#
#
# validation regions
#
#

reg_1lep3_5jwin1bex0fjex0Tex0Lex0Hex1Vin = {
    'name':"HTX_c1lep3_5jwin1bex0fjex0Tex0Lex0Hex1Vin",
    'legend': "1l, 3-5j, 1b, 0fj, 0h, #geq1v, 0t_{h}, 0t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3_5jwin1bex0fjex0Tex1Lin0Hex1Vin = {
    'name':"HTX_c1lep3_5jwin1bex0fjex0Tex1Lin0Hex1Vin",
    'legend': "1l, 3-5j, 1b, 0fj, 0h, #geq1v, 0t_{h}, #geq1t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3_5jwin1bex1fjin1LTin0Hex0Vex = {
    'name':"HTX_c1lep3_5jwin1bex1fjin1LTin0Hex0Vex",
    'legend': "1l, 3-5j, 1b, #geq1fj, 0h, 0v, #geq1t_{h}+t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3_5jwin1bex1fjin1Tin0Lex0Hex1Vin = {
    'name':"HTX_c1lep3_5jwin1bex1fjin1Tin0Lex0Hex1Vin",
    'legend': "1l, 3-5j, 1b, #geq1fj, 0h, #geq1v, #geq1t_{h}, 0t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3_5jwin2bex0fjex0Tex0Lex0Hex1Vin = {
    'name':"HTX_c1lep3_5jwin2bex0fjex0Tex0Lex0Hex1Vin",
    'legend': "1l, 3-5j, 2b, 0fj, 0h, #geq1v, 0t_{h}, 0t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3_5jwin2bex0fjex0Tex1Lin0Hex1Vin = {
    'name':"HTX_c1lep3_5jwin2bex0fjex0Tex1Lin0Hex1Vin",
    'legend': "1l, 3-5j, 2b, 0fj, 0h, #geq1v, 0t_{h}, #geq1t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3_5jwin2bex1fjin1LTin0Hex0Vex = {
    'name':"HTX_c1lep3_5jwin2bex1fjin1LTin0Hex0Vex",
    'legend': "1l, 3-5j, 2b, #geq1fj, 0h, 0v, #geq1t_{h}+t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3_5jwin2bex1fjin1Tin0Lex0Hex1Vin = {
    'name':"HTX_c1lep3_5jwin2bex1fjin1Tin0Lex0Hex1Vin",
    'legend': "1l, 3-5j, 2b, #geq1fj, 0h, #geq1v, #geq1t_{h}, 0t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3_5jwin3bex0fjex0Tex0Lex1Hin0Vex = {
    'name':"HTX_c1lep3_5jwin3bex0fjex0Tex0Lex1Hin0Vex",
    'legend': "1l, 3-5j, 3b, 0fj, #geq1h, 0v, 0t_{h}, 0t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3_5jwin3bex0fjex0Tex1Lin1Hin0Vex = {
    'name':"HTX_c1lep3_5jwin3bex0fjex0Tex1Lin1Hin0Vex",
    'legend': "1l, 3-5j, 3b, 0fj, #geq1h, 0v, 0t_{h}, #geq1t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3_5jwin3bex1fjin1Tin0Lex1Hin0Vex = {
    'name':"HTX_c1lep3_5jwin3bex1fjin1Tin0Lex1Hin0Vex",
    'legend': "1l, 3-5j, 3b, #geq1fj, #geq1h, 0v, #geq1t_{h}, 0t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3_5jwin3bex1fjin1VLTin0Hex = {
    'name':"HTX_c1lep3_5jwin3bex1fjin1VLTin0Hex",
    'legend': "1l, 3-5j, 3b, #geq1fj, 0h, #geq1v+t_{l}+t_{h}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3_5jwin4bin0fjex0Tex0Lex1Hin0Vex = {
    'name':"HTX_c1lep3_5jwin4bin0fjex0Tex0Lex1Hin0Vex",
    'legend': "1l, 3-5j, #geq4b, 0fj, #geq1h, 0v, 0t_{h}, 0t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3_5jwin4bin0fjex0Tex1Lin1Hin0Vex = {
    'name':"HTX_c1lep3_5jwin4bin0fjex0Tex1Lin1Hin0Vex",
    'legend': "1l, 3-5j, #geq4b, 0fj, #geq1h, 0v, 0t_{h}, #geq1t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3_5jwin4bin1fjin1Tin0Lex1Hin0Vex = {
    'name':"HTX_c1lep3_5jwin4bin1fjin1Tin0Lex1Hin0Vex",
    'legend': "1l, 3-5j, #geq4b, #geq1fj, #geq1h, 0v, #geq1t_{h}, 0t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3_5jwin4bin1fjin1VLTin0Hex = {
    'name':"HTX_c1lep3_5jwin4bin1fjin1VLTin0Hex",
    'legend': "1l, 3-5j, #geq4b, #geq1fj, 0h, #geq1v+t_{l}+t_{h}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin1bex0fjex1LTex0Hex1Vin = {
    'name':"HTX_c1lep6jin1bex0fjex1LTex0Hex1Vin",
    'legend': "1l, #geq6j, 1b, 0fj, 0h, #geq1v, 1t_{l}+t_{h}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin1bex0fjex2LTin0Hex1Vin = {
    'name':"HTX_c1lep6jin1bex0fjex2LTin0Hex1Vin",
    'legend': "1l, #geq6j, 1b, 0fj, 0h, #geq1v, #geq2t_{h}+t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin1bex1fjin0Tex0Lex1Hin1Vin = {
    'name':"HTX_c1lep6jin1bex1fjin0Tex0Lex1Hin1Vin",
    'legend': "1l, #geq6j, 1b, #geq1fj, #geq1h, #geq1v, 0t_{h}, 0t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin1bex1fjin2LTin1Hin0Vex = {
    'name':"HTX_c1lep6jin1bex1fjin2LTin1Hin0Vex",
    'legend': "1l, #geq6j, 1b, #geq1fj, #geq1h, 0v, #geq2t_{h}+t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin2bex0fjex1LTex0Hex1Vin = {
    'name':"HTX_c1lep6jin2bex0fjex1LTex0Hex1Vin",
    'legend': "1l, #geq6j, 2b, 0fj, 0h, #geq1v, 1t_{l}+t_{h}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin2bex0fjex2LTin0Hex1Vin = {
    'name':"HTX_c1lep6jin2bex0fjex2LTin0Hex1Vin",
    'legend': "1l, #geq6j, 2b, 0fj, 0h, #geq1v, #geq2t_{h}+t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin2bex1fjin0Tex0Lex1Hin1Vin = {
    'name':"HTX_c1lep6jin2bex1fjin0Tex0Lex1Hin1Vin",
    'legend': "1l, #geq6j, 2b, #geq1fj, #geq1h, #geq1v, 0t_{h}, 0t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin2bex1fjin2LTin1Hin0Vex = {
    'name':"HTX_c1lep6jin2bex1fjin2LTin1Hin0Vex",
    'legend': "1l, #geq6j, 2b, #geq1fj, #geq1h, 0v, #geq2t_{h}+t_{l}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin3bex0fjex1VLTex1Hin = {
    'name':"HTX_c1lep6jin3bex0fjex1VLTex1Hin",
    'legend': "1l, #geq6j, 3b, 0fj, #geq1h, 1v+t_{l}+t_{h}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin3bex0fjex2VLTin1Hin = {
    'name':"HTX_c1lep6jin3bex0fjex2VLTin1Hin",
    'legend': "1l, #geq6j, 3b, 0fj, #geq1h, #geq2v+t_{l}+t_{h}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin3bex1fjin1VLTex0Hex = {
    'name':"HTX_c1lep6jin3bex1fjin1VLTex0Hex",
    'legend': "1l, #geq6j, 3b, #geq1fj, 0h, 1v+t_{l}+t_{h}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin3bex1fjin2VLTin0Hex = {
    'name':"HTX_c1lep6jin3bex1fjin2VLTin0Hex",
    'legend': "1l, #geq6j, 3b, #geq1fj, 0h, #geq2v+t_{l}+t_{h}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin4bin0fjex1VLTex1Hin = {
    'name':"HTX_c1lep6jin4bin0fjex1VLTex1Hin",
    'legend': "1l, #geq6j, #geq4b, 0fj, #geq1h, 1v+t_{l}+t_{h}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin4bin0fjex2VLTin1Hin = {
    'name':"HTX_c1lep6jin4bin0fjex2VLTin1Hin",
    'legend': "1l, #geq6j, #geq4b, 0fj, #geq1h, #geq2v+t_{l}+t_{h}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin4bin1fjin1VLTex0Hex = {
    'name':"HTX_c1lep6jin4bin1fjin1VLTex0Hex",
    'legend': "1l, #geq6j, #geq4b, #geq1fj, 0h, 1v+t_{l}+t_{h}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin4bin1fjin2VLTin0Hex = {
    'name':"HTX_c1lep6jin4bin1fjin2VLTin0Hex",
    'legend': "1l, #geq6j, #geq4b, #geq1fj, 0h, #geq2v+t_{l}+t_{h}",
    'binning':"600,800,1000,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}

#
#
# Preselection regions
#
#

reg_1lep3_5jwin1bin = {
    'name':"HTX_c1lep3_5jwin1bin",
    'legend': "3-5j, #geq1b",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
    # 'type':"SIGNAL" # TEMP to make TRExF work with only presel regions
}
reg_1lep3_5jwin2bin = {
  'name':"HTX_c1lep3_5jwin2bin",
    'legend': "3-5j, #geq2b",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3_5jwin3bin = {
  'name':"HTX_c1lep3_5jwin3bin",
    'legend': "3-5j, #geq3b",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3jin1bin = {
    'name':"HTX_c1lep3jin1bin",
    'legend': "#geq3j, #geq1b",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3jin2bin = {
    'name':"HTX_c1lep3jin2bin",
    'legend': "#geq3j, #geq2b",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep3jin3bin = {
    'name':"HTX_c1lep3jin3bin",
    'legend': "#geq3j, #geq3b",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep5jin1bin = {
    'name':"HTX_c1lep5jin1bin",
    'legend': "#geq5j, #geq1b",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep5jin2bin = {
    'name':"HTX_c1lep5jin2bin",
    'legend': "#geq5j, #geq2b",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep5jin3bin = {
    'name':"HTX_c1lep5jin3bin",
    'legend': "#geq5j, #geq3b",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin1bin = {
    'name':"HTX_c1lep6jin1bin",
    'legend': "#geq6j, #geq1b",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin2bin = {
    'name':"HTX_c1lep6jin2bin",
    'legend': "#geq6j, #geq2b",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_1lep6jin3bin = {
    'name':"HTX_c1lep6jin3bin",
    'legend': "#geq6j, #geq3b",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}
reg_all = {
    'name':"HTX_call",
    'legend': "No selection",
    'binning':"600,700,800,900,1000,1200,1400,1600,1800,3000",
    'binning_recoVLQ0_m':"600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2200,3000",
    'type':"VALIDATION"
}

fit_regions_1l = [

    reg_1lep3_5jwin1bex1fjin0Tex1Lin0Hex1Vin,	
    reg_1lep3_5jwin2bex1fjin0Tex1Lin0Hex1Vin,	
    reg_1lep3_5jwin1bex1fjin0LTex0Hex1Vin,	
    reg_1lep3_5jwin2bex1fjin0LTex0Hex1Vin,	
    reg_1lep3_5jwin3bex1fjin0Tex1Lin1Hin0Vex,	
    reg_1lep3_5jwin4bin1fjin0Tex1Lin1Hin0Vex,	
    reg_1lep3_5jwin3bex1fjin0LTex1Hin0Vex,	
    reg_1lep3_5jwin4bin1fjin0LTex1Hin0Vex,	
    
    reg_1lep6jin1bex1fjin1Lex0Tex0Hex1Vin,	
    reg_1lep6jin2bex1fjin1Lex0Tex0Hex1Vin,	
    reg_1lep6jin1bex1fjin0Lex1Tex0Hex1Vin,	
    reg_1lep6jin2bex1fjin0Lex1Tex0Hex1Vin,	
    reg_1lep6jin1bex1fjin2LTin0Hex1Vin,
    reg_1lep6jin2bex1fjin2LTin0Hex1Vin,
    reg_1lep6jin3bex1fjin0Lex1VTex1Hin,	
    reg_1lep6jin4bin1fjin0Lex1VTex1Hin,	
    reg_1lep6jin3bex1fjin1Lex0VTex1Hin,	
    reg_1lep6jin4bin1fjin1Lex0VTex1Hin,	
    reg_1lep6jin3bex1fjin2VLTin1Hin,	
    reg_1lep6jin4bin1fjin2VLTin1Hin	
]

validation_regions_1l = [

    reg_1lep3_5jwin1bex0fjex0Tex0Lex0Hex1Vin,
    reg_1lep3_5jwin1bex0fjex0Tex1Lin0Hex1Vin,
    reg_1lep3_5jwin1bex1fjin1LTin0Hex0Vex,
    reg_1lep3_5jwin1bex1fjin1Tin0Lex0Hex1Vin,
    reg_1lep3_5jwin2bex0fjex0Tex0Lex0Hex1Vin,
    reg_1lep3_5jwin2bex0fjex0Tex1Lin0Hex1Vin,
    reg_1lep3_5jwin2bex1fjin1LTin0Hex0Vex,
    reg_1lep3_5jwin2bex1fjin1Tin0Lex0Hex1Vin,
    reg_1lep3_5jwin3bex0fjex0Tex0Lex1Hin0Vex,
    reg_1lep3_5jwin3bex0fjex0Tex1Lin1Hin0Vex,
    reg_1lep3_5jwin3bex1fjin1Tin0Lex1Hin0Vex,
    reg_1lep3_5jwin3bex1fjin1VLTin0Hex,
    reg_1lep3_5jwin4bin0fjex0Tex0Lex1Hin0Vex,
    reg_1lep3_5jwin4bin0fjex0Tex1Lin1Hin0Vex,
    reg_1lep3_5jwin4bin1fjin1Tin0Lex1Hin0Vex,
    reg_1lep3_5jwin4bin1fjin1VLTin0Hex,
    
    reg_1lep6jin1bex0fjex1LTex0Hex1Vin,
    reg_1lep6jin1bex0fjex2LTin0Hex1Vin,
    reg_1lep6jin1bex1fjin0Tex0Lex1Hin1Vin,
    reg_1lep6jin1bex1fjin2LTin1Hin0Vex,
    reg_1lep6jin2bex0fjex1LTex0Hex1Vin,
    reg_1lep6jin2bex0fjex2LTin0Hex1Vin,
    reg_1lep6jin2bex1fjin0Tex0Lex1Hin1Vin,
    reg_1lep6jin2bex1fjin2LTin1Hin0Vex,
    reg_1lep6jin3bex0fjex1VLTex1Hin,
    reg_1lep6jin3bex0fjex2VLTin1Hin,
    reg_1lep6jin3bex1fjin1VLTex0Hex,
    reg_1lep6jin3bex1fjin2VLTin0Hex,
    reg_1lep6jin4bin0fjex1VLTex1Hin,
    reg_1lep6jin4bin0fjex2VLTin1Hin,
    reg_1lep6jin4bin1fjin1VLTex0Hex,
    reg_1lep6jin4bin1fjin2VLTin0Hex
]

preselection_regions_1l = [

    reg_1lep3_5jwin1bin,
    reg_1lep3_5jwin2bin,
    reg_1lep3_5jwin3bin,
    reg_1lep3jin1bin,
    reg_1lep3jin2bin,
    reg_1lep3jin3bin,
    reg_1lep5jin1bin,
    reg_1lep5jin2bin,
    reg_1lep5jin3bin,
    reg_1lep6jin1bin,
    reg_1lep6jin2bin,
    reg_1lep6jin3bin,
    reg_all
]

all_regions_1l =  []
all_regions_1l += fit_regions_1l
all_regions_1l += validation_regions_1l
all_regions_1l += preselection_regions_1l
