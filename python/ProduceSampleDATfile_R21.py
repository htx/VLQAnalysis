#!/bin/python
import sys
import os
import string
import time, getpass
import socket
import datetime
import json
from ROOT import *
from VLQ_Samples import *

sys.path.append( os.getenv("ROOTCOREBIN") + "python/IFAETopFramework/" )
from BatchTools import *
from Job import *
from Samples import *

##________________________________________________________
## OPTIONS

listProc=[]
xSecFilePath=""
sumWeightsFilePath=""
inputDir=""
procDir=""
mcCampaigns=["mc16a","mc16d","mc16e"]
mergeFiles=True
mergeCampaigns=True
debug=True
queryXSec=True
sumFiles=True

nFilesSplit = 1000
nMerge=1

##........................................................

if(len(sys.argv))>1:
    for x in sys.argv[1:]:
        splitted=x.split("=")
        if(len(splitted)!=2):
            printError("<!> The argument \"" + x + "\" has no equal signs ... Please check")
            sys.exit(-1)
        argument = splitted[0].upper().replace("--","")
        value = splitted[1]
        if(argument=="LISTPROC"):
            listProc=value.split(",")
        elif(argument=="XSECFILE"):
            xSecFilePath=value
        elif(argument=="SUMWEIGHTSFILE"):
            sumWeightsFilePath=value
        elif(argument=="INPUTDIR"):
            inputDir=value
        elif(argument=="PROCDIR"):
            procDir=value
        elif(argument=="CAMPAIGN"):
            mcCampaigns=value.split(",")
        elif(argument=="DEBUG"):
            debug=(value.upper()=="TRUE")
        elif(argument=="QUERYXSEC"):
            queryXSec=(value.upper()=="TRUE")
        elif(argument=="SUMFILES"):
            sumFiles=(value.upper()=="TRUE")
        else:
            printError("Unknown argument : "+argument)
else:
    printError("<!> No arguments seen ... Aborting !")
    sys.exit(-1)

if(len(listFolder)==0):
    queryXSec = True
    sumFiles = True

if(len(listProc)==0):
    printError("<!> No process list specified ... Aborting !")
    sys.exit(-1)
if not inputDir:
    printError("<!> No input directory specified ... Aborting !")
    sys.exit(-1)


##________________________________________________________
## Defines some useful variables
platform = socket.gethostname()
now = datetime.datetime.now().strftime("%Y_%m_%d_%H%M")
here = os.getcwd()

#mcPrefix='mc16_13TeV.'
#lenDSID=6
##........................................................

##________________________________________________________
## Defining the paths and the tarball
if len(listFolder)==0:
    listFolder=here+"/Lists_Analysis_" + now
histogramName="cut_flow"
##........................................................

##________________________________________________________
## Creating useful repositories
os.system("mkdir -p " + listFolder) #list files folder
##........................................................

def MakeXSecFile():



    # Merge all provided processes for each mc campaign into one text file and run metaData query
    # Then, open the output file and read in the xsec for each DSID
    sampleMetaList = {}
    sortedFiles = {}
    DSset = set()

    for campaign in mcCampaigns:

        metaFilePath=listFolder+"/XSec_"+campaign+".list"
        sampleMetaList[campaign] = {}
        sortedFiles[campaign] = {}
        
        DSlist=listFolder+"/DSlist_"+campaign+".list"
        
        with open(DSlist, 'w') as DSfile:

            for proc in listProc:

                dspath=procDir+'/'+campaign+'/'+proc+'_'+campaign+'.list'
                with open(dspath) as procdsfile:
                    for line in procdsfile:
                        if not line.startswith("mc16_13TeV"):
                            continue
                        DSfile.write(line)


        com="getMetadata.py --inDsTxt="
        com+=DSlist
        com+=" --outFile="
        com+=metaFilePath
        
        print "Calling " + com    

        result=os.system(com)
        print "After metadata query"

        #convert metaFile into a python dictionary
        with open(metaFilePath) as metaFile:
            for line in metaFile:

                #remove whitespace
                line=line.strip()
                #remove empty line
                if(len(line)==0):
                    continue
                #first column is DSID
                #multiply next three columns
                if("dataset_number/I:crossSection/D:kFactor/D:genFiltEff/D" in line):
                    continue
                if(line.startswith("#")):
                    continue
                if(len(line.split())!=4):
                    printWarning(" Ouput of getMetadata has unexpected format. Please check line \n "+line)
                    continue
                dsid=line.split()[0]

                try:
                    xsec = float(line.split()[1])*1000.
                except:
                    printWarning(" WARNING: dsid "+dsid+" has xsec "+line.split()[1]+". \n Setting to -1.")
                    xsec = -1.

                try:
                    kFactor = float(line.split()[1])*1000.
                except:
                    printWarning(" WARNING: dsid "+dsid+" has kFactor "+line.split()[1]+". \n Setting to -1.")
                    kFactor = -1.

                try:
                    genFiltEff = float(line.split()[1])*1000.
                except:
                    printWarning(" WARNING: dsid "+dsid+" has genFiltEff "+line.split()[1]+". \n Setting to -1.")
                    genFiltEff = -1.

                DSset.add(dsid)
                sampleMeta = { "dsid": line.split()[0],
                               "xsec": xsec,
                               "kFactor": kFactor,
                               "genFiltEff": genFiltEff
                           }
                sampleMetaList[campaign][dsid] = sampleMeta
                sortedFiles[campaign][dsid] = []

    print " All xsec written "
    return

#......................................................................

    

def MakeSumWeightsFile():


    # Produce the total files list if the option useTotalFileFirst is set to True (avoids doing ls multiple times)
    # Also keep the option to update single DSID, mc campaign, or process lists by putting in matching patterns

    #Output datasets are of the format user.[username].DSID.* or group.[groupname].DSID.*

    totalFileName = "AllFiles_"+inputDir.replace("/","_")
    if not os.path.exists(listFolder + "/" + totalFileName ) :
        failed = produceList(listProc,inputDir,listFolder + "/" + totalFileName,)
        if failed>=1 :
            printError("I didn't find any ROOT files in directory ... Aborting !")
            sys.exit(-1)

    with open(listFolder + "/" + totalFileName) as f:
        for line in f:
            if not line.startswith(inputDir):
                continue

            print "line", line
            #extract the DS directory
            DSdir=line[len(inputDir):len(line)]
            DSdir=DSdir[0:DSdir.find("/")]

            #Remove 'user' or 'group' prefix
            dsid=DSdir[DSdir.find(".")+1:len(DSdir)]
            #Remove [username] or [groupname]
            dsid=dsid[dsid.find(".")+1:len(dsid)]

            #Remove everything after "."
            dsid=dsid[0:dsid.find(".")]
            print "dsid : ", dsid

            if not dsid in DSset:
                continue

            campaign=""

            if('_mc16a.' in line):
                campaign="mc16a"
            elif('_mc16d.' in line):
                campaign="mc16d"
            elif('_mc16e.' in line):
                campaign="mc16e"
            else:
                printWarning("Unknown campaign. Skipping file.")
                continue
        
            if not campaign in mcCampaigns:
                continue

            if dsid in sortedFiles[campaign]:
                sortedFiles[campaign][dsid].append(line)

    #So now we have sampleMetaList[campaign][dsid] and sortedFiles[campaign][dsid] 
    
    print "Summed files"

    nEventsWeighted={}
    strCampaigns=""

    for campaign in mcCampaigns:

        nEventsWeighted[campaign]={}
        strCampaigns += campaign + "_"

        ##________________________________________________________
        ## Creating the config file
        #configFile = open("configFile_"+campaign+"_"+now+".dat","w")
        #configFile.write("#\n#\n")
        #configFile.write("# Path to files: " + inputDir + "\n")
    configFile.write("# Date: " + now + "\n")
    configFile.write("# Histogram used to normalize: "+ histogramName +"\n")
    configFile.write("#\n#\n")

    print " Nsamples : ", len(sampleMetaList[campaign])
    for dsid in sampleMetaList[campaign]:
        sample = sampleMetaList[campaign][dsid]

        #print campaign, "_", dsid, "xsec : ", sample["xsec"], " kFactor", sample["kFactor"], " genFiltEff", sample["genFiltEff"]

        effXsec=float(sample["xsec"]*sample["kFactor"]*sample["genFiltEff"])

        nEventsWeighted[campaign][dsid]=0
        fileList=sortedFiles[campaign][dsid]

        for f in fileList:
            root_f = TFile.Open(f.strip(),"read")

            if not root_f or root_f.IsZombie():
                printError("<!> The file "+f+" is corrupted ... removes it from the list")
                continue
                        
            h_nEvents = root_f.Get(histogramName).Clone()
            h_nEvents.SetDirectory(0)
            nEventsWeighted[campaign][dsid] += h_nEvents.GetBinContent(2)

        #Print the information out to samples_info.dat
        configFile.write(dsid+" "+`nEventsWeighted[campaign][dsid]`+" "+`effXsec`+"\n")
        if effXsec < 0:
            printError("<!> The sample "+ dsid +" is not in the cross-section file. Please check ! The cross-section is set to -1.")
            continue

    configFile.close()

#.............................................................
#Merge mc campaigns 

##________________________________________________________
## Creating the config file for merged MC campaigns
configFileMerged = open("configFileMerged_"+strCampaigns+now+".dat","w")
configFileMerged.write("#\n#\n")
configFileMerged.write("# Path to files: " + inputDir + "\n")
configFileMerged.write("# Date: " + now + "\n")
configFileMerged.write("# Histogram used to normalize: "+ histogramName +"\n")
configFileMerged.write("#\n#\n")

for dsid in DSset:

    effXsec = -1.
    nEventsWeightedAll=0

    for campaign in mcCampaigns:
        if(effXsec == -1. and (dsid in sampleMetaList[campaign])):
            sample=sampleMetaList[campaign][dsid]
            effXsec=float(sample["xsec"]*sample["kFactor"]*sample["genFiltEff"])
        nEventsWeightedAll += nEventsWeighted[campaign][dsid]

    #Print the information out to samples_info.dat
    configFileMerged.write(dsid+" "+`nEventsWeightedAll`+" "+`effXsec`+"\n")
    if effXsec < 0:
        printError("<!> The sample "+ dsid +" is not in the cross-section file. Please check ! The cross-section is set to -1.")
        continue

configFileMerged.close()




##________________________________________________________
## Removing list folder
os.system("rm -rf " + listFolder) #list files folder
##........................................................
