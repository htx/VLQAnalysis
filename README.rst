Checking Out
============

To checkout, grab all necessary packages::

  rcSetup Base,2.4.24
  git clone ssh://git@gitlab.cern.ch:7999/htx/IFAETopFramework.git

  git clone ssh://git@gitlab.cern.ch:7999/htx/BtaggingTRFandRW.git

  git clone ssh://git@gitlab.cern.ch:7999/htx/IFAEReweightingTools.git
  cd IFAEReweightingTools && git checkout IFAEReweightingTools-00-01-06-07 && cd ..

  git clone ssh://git@gitlab.cern.ch:7999/htx/FakeLeptonEstimation.git;
  cd FakeLeptonEstimation && git checkout v0.2 && cd ..

  git clone ssh://git@gitlab.cern.ch:7999/htx/VLQAnalysis.git


The compilation of the packages (can be about 5 minutes long due to the TRF package) is done as follows::

  rc find_packages
  rc clean
  rc compile

An example line of code to execute is::


    VLQAnalysis \
    --outputFile=ttbar.root \
    --inputFile=/nfs/at3/scratch2/lvalery/VLQFiles/MBJ-2.4.28-2/group.phys-exotics.410000.ttbar.DAOD_TOPQ1.e3698_s2608_s2183_r7725_r7676_p2669_2.4.28-3-0_output_tree.root/group.phys-exotics.10941921._000001.output_tree.root \
    --textFileList=false --sampleName=ttbar --sampleID=410000. \
    --inputTree=nominal --isData=false \
    --dumpHistos=true --dumpTree=false --dumpOverlapTree=false --doBlind=false \
    --splitVLQDecays=true --doTruthAnalysis=false --filterType=NOFILTER \
    --otherVariables=false --computeWeightSys=false \
    --recomputeTtbbRW=false --computeTtccNLO=false --RWTtFractions=false \
    --useLeptonsSF=true --usePUWeight=true \
    --doTRF=false --recomputeTRF=false --btagOP=77 \
    --useLeptonTrigger=true --useMETTrigger=true \
    --doOneLeptonAna=true --doZeroLeptonAna=true \
    --doLowBRegions=false --doLowJRegions=false \
    --doFitRegions=false --doValidnRegions=false \
    --doSingleVLQPreselection=true --doPairVLQPreselection=false \
    --doExtendedPreselection=false --doSingleVLQRegions=false --doPairVLQRegions=false \
    --doFCNCRegions=false --doHBSMRegions=false \
    --applyMetMtwCuts=true --invertMetMtwCuts=false \
    --applydeltaphicut=true --invertdeltaphicut=false \
    --jetPtCut=25 --RCJetPtCut=300 --RCNSUBJETSCUT=2 --isR21=true
