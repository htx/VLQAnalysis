% -------------------------- %
% -------    JOB     ------- %
% -------------------------- %

Job: "__FITTYPE_____SIGNAL_____ADDITION__"
Label: "Ht+X analysis"
CmeLabel: "13 TeV"
LumiLabel: "139.02 fb^{-1}"
Lumi: __LUMIVALUE__
POI: "mu_signal"
ReadFrom: HIST
HistoPath: "__HISTOPATH__"
DebugLevel: 0
SystControlPlots: FALSE
SystPruningShape: 0.01
SystPruningNorm: 0.01
CorrelationThreshold: 0.20
HistoChecks: NOCRASH
SplitHistoFiles: TRUE
MCstatThreshold: 0.05
StatOnly: __STATONLY__
SystLarge: 10.
GetChi2: FALSE
%PlotOptions: "CHI2"

% -------------------------- %
% -------    FIT     ------- %
% -------------------------- %

Fit: "__FITTYPE__"
FitType: __FITTYPE__
FitRegion: __FITREGION__
FitBlind: __FITBLIND__
POIAsimov: __FITPOIASIMOV__

% --------------------------- %
% ---------- LIMIT ---------- %
% --------------------------- %

Limit: "limit"
LimitType: ASYMPTOTIC
LimitBlind: __LIMITBLIND__
POIAsimov: __LIMITPOIASIMOV__

% -------------------------- %
% --------- REGIONS -------- %
% -------------------------- %

_REGIONLIST_

% --------------------------- %
% --------  SAMPLES  -------- %
% --------------------------- %

Sample: "ttbarlight"
Title: "t#bar{t}+light"
Type: background
FillColor: 0
LineColor: 1
HistoFile: "ttbarlight"

Sample: "ttbarcc"
Title: "t#bar{t}+cc"
Type: background
FillColor: 590
LineColor: 1
HistoFile: "ttbarcc"

Sample: "ttbarbb"
Title: "t#bar{t}+bb"
Type: background
FillColor: 594
LineColor: 1
HistoFile: "ttbarbb"

Sample: "topEW"
Title: "topEW"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "topEW"
Group: "Non-t#bar{t}"

Sample: "ttH"
Title: "ttH"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "ttH"
Group: "Non-t#bar{t}"

Sample: "Wjets"
Title: "W+jets"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Wjets"
Group: "Non-t#bar{t}"

#Sample: "Wjetslight"
#Title: "W+jetslight"
#Type: background
#FillColor: 410
#LineColor: 1
#HistoFile: "Wjets22light"
#Group: "Non-t#bar{t}"

#Sample: "Wjetscharm"
#Title: "W+jetscharm"
#Type: background
#FillColor: 410
#LineColor: 1
#HistoFile: "Wjets22charm"
#Group: "Non-t#bar{t}"

#Sample: "Wjetsbeauty"
#Title: "W+jetsbeauty"
#Type: background
#FillColor: 410
#LineColor: 1
#HistoFile: "Wjets22beauty"
#Group: "Non-t#bar{t}"

Sample: "Zjets"
Title: "Z+jets"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Zjets"
Group: "Non-t#bar{t}"

#Sample: "Zjetslight"
#Title: "Z+jetslight"
#Type: background
#FillColor: 410
#LineColor: 1
#HistoFile: "Zjets22light"
#Group: "Non-t#bar{t}"

#Sample: "Zjetscharm"
#Title: "Z+jetscharm"
#Type: background
#FillColor: 410
#LineColor: 1
#HistoFile: "Zjets22charm"
#Group: "Non-t#bar{t}"

#Sample: "Zjetsbeauty"
#Title: "Z+jetsbeauty"
#Type: background
#FillColor: 410
#LineColor: 1
#HistoFile: "Zjets22beauty"
#Group: "Non-t#bar{t}"

Sample: "Singletop"
Title: "Single-top"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Singletop"
Group: "Non-t#bar{t}"

Sample: "Dibosons"
Title: "Dibosons"
Type: background
FillColor: 410
LineColor: 1
HistoFile: "Dibosons"
Group: "Non-t#bar{t}"

#Sample: "QCD"
#Title: "QCD"
#Type: background
#FillColor: 410
#LineColor: 1
#HistoFile: "QCD"
#Group: "Non-t#bar{t}"
#NormalizedByTheory: FALSE
#Regions: HTX_c1lep*

#Sample: "QCD0L"
#Title: "QCD0L"
#Type: background
#FillColor: 410
#LineColor: 1
#HistoFile: "QCD0L"
#Group: "Non-t#bar{t}"
#NormalizedByTheory: FALSE
#Regions: HTX_c0lep*

#NormFactor: "HTX_BKGNORM_TTBARBB"
#Title: "t#bar{t}+#geq1b normalisation"
#Nominal: 1
#Min: -10
#Max: 10
#Samples: ttbarbb
