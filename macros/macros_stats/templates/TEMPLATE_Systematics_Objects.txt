
% ------------------------------ %
% --------   ELECTRONS  -------- %
% ------------------------------ %

Systematic: "EG_RESOLUTION_ALL"
  Title: "Electron E_{T} res."
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoFileSufUp: _EG_RESOLUTION_ALL__1up
  HistoFileSufDown: _EG_RESOLUTION_ALL__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L
  Regions: HTX_c1lep*

Systematic: "EG_SCALE_ALL"
  Title: "Electron E_{T} scale"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoFileSufUp: _EG_SCALE_ALL__1up
  HistoFileSufDown: _EG_SCALE_ALL__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L
  Regions: HTX_c1lep*

% ------------------------------ %
% ----------   MUONS  ---------- %
% ------------------------------ %

Systematic: "MUON_ID"
  Title: "Muon ID p_{T} res."
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoFileSufUp: _MUON_ID__1up
  HistoFileSufDown: _MUON_ID__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L
  Regions: HTX_c1lep*

Systematic: "MUON_MS"
  Title: "Muon MS p_{T} res."
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoFileSufUp: _MUON_MS__1up
  HistoFileSufDown: _MUON_MS__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L
  Regions: HTX_c1lep*

Systematic: "MUON_SCALE"
  Title: "Muon p_{T} scale"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoFileSufUp: _MUON_SCALE__1up
  HistoFileSufDown: _MUON_SCALE__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L
  Regions: HTX_c1lep*

Systematic: "MUON_SAGITTA_RESBIAS"
  Title: "Muon p_{T} scale"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoFileSufUp: _MUON_SAGITTA_RESBIAS__1up
  HistoFileSufDown: _MUON_SAGITTA_RESBIAS__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L
  Regions: HTX_c1lep*

Systematic: "MUON_SAGITTA_RHO"
  Title: "Muon p_{T} scale"
  Type: HISTO
  Category: "Lepton uncertainties"
  HistoFileSufUp: _MUON_SAGITTA_RHO__1up
  HistoFileSufDown: _MUON_SAGITTA_RHO__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L
  Regions: HTX_c1lep*

% ------------------------------ %
% -----------   JETS  ---------- %
% ------------------------------ %

Systematic: "JET_BJES_Response"
  Title: "b JES response"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_BJES_Response__1up
  HistoFileSufDown: _JET_BJES_Response__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_EffectiveNP_1"
  Title: "Effective NP 1"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EffectiveNP_1__1up
  HistoFileSufDown: _JET_EffectiveNP_1__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_EffectiveNP_2"
  Title: "Effective NP 2"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EffectiveNP_2__1up
  HistoFileSufDown: _JET_EffectiveNP_2__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_EffectiveNP_3"
  Title: "Effective NP 3"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EffectiveNP_3__1up
  HistoFileSufDown: _JET_EffectiveNP_3__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_EffectiveNP_4"
  Title: "Effective NP 4"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EffectiveNP_4__1up
  HistoFileSufDown: _JET_EffectiveNP_4__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_EffectiveNP_5"
  Title: "Effective NP 5"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EffectiveNP_5__1up
  HistoFileSufDown: _JET_EffectiveNP_5__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_EffectiveNP_6"
  Title: "Effective NP 6"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EffectiveNP_6__1up
  HistoFileSufDown: _JET_EffectiveNP_6__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_EffectiveNP_7"
  Title: "Effective NP 7"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EffectiveNP_7__1up
  HistoFileSufDown: _JET_EffectiveNP_7__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_EffectiveNP_8restTerm"
  Title: "Effective NP 8"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EffectiveNP_8restTerm__1up
  HistoFileSufDown: _JET_EffectiveNP_8restTerm__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_EtaIntercalibration_Modelling"
  Title: "#eta intercalib. (model)"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EtaIntercalibration_Modelling__1up
  HistoFileSufDown: _JET_EtaIntercalibration_Modelling__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_EtaIntercalibration_TotalStat"
  Title: "#eta intercalib. (stat.)"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EtaIntercalibration_TotalStat__1up
  HistoFileSufDown: _JET_EtaIntercalibration_TotalStat__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_EtaIntercalibration_NonClosure"
  Title: "#eta intercalib. (stat.)"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_EtaIntercalibration_NonClosure__1up
  HistoFileSufDown: _JET_EtaIntercalibration_NonClosure__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_Flavor_Composition"
  Title: "Flavour composition"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_Flavor_Composition__1up
  HistoFileSufDown: _JET_Flavor_Composition__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_Flavor_Response"
  Title: "Flavor response"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_Flavor_Response__1up
  HistoFileSufDown: _JET_Flavor_Response__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_Pileup_OffsetMu"
  Title: "Pile-up offset mu term"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_Pileup_OffsetMu__1up
  HistoFileSufDown: _JET_Pileup_OffsetMu__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_Pileup_OffsetNPV"
  Title: "Pile-up offset NPV term"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_Pileup_OffsetNPV__1up
  HistoFileSufDown: _JET_Pileup_OffsetNPV__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_Pileup_PtTerm"
  Title: "Pile-up offset p_{T} term"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_Pileup_PtTerm__1up
  HistoFileSufDown: _JET_Pileup_PtTerm__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_Pileup_RhoTopology"
  Title: "#rho topology"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_Pileup_RhoTopology__1up
  HistoFileSufDown: _JET_Pileup_RhoTopology__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_PunchThrough_MC15"
  Title: "Punch-through correction"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_PunchThrough_MC15__1up
  HistoFileSufDown: _JET_PunchThrough_MC15__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_SingleParticle_HighPt"
  Title: "High-p_{T} term"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_SingleParticle_HighPt__1up
  HistoFileSufDown: _JET_SingleParticle_HighPt__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_Rtrk_Baseline_mass"
  Title: "JMS baseline"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_Rtrk_Baseline_mass__1up
  HistoFileSufDown: _JET_Rtrk_Baseline_mass__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_Rtrk_Modelling_mass"
  Title: "JMS modelling"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_Rtrk_Modelling_mass__1up
  HistoFileSufDown: _JET_Rtrk_Modelling_mass__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_Rtrk_TotalStat_mass"
  Title: "JMS stat."
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_Rtrk_TotalStat_mass__1up
  HistoFileSufDown: _JET_Rtrk_TotalStat_mass__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_Rrk_Tracking_mass"
  Title: "JMS tracking"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_Rrk_Tracking_mass__1up
  HistoFileSufDown: _JET_Rrk_Tracking_mass__1down
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "JET_JER_SINGLE_NP"
  Title: "JER"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_JER_SINGLE_NP__1up
  Symmetrisation: "OneSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "HTX_JET_JMR__1up"
  Title: "JMR"
  Type: HISTO
  Category: "Jet uncertainties"
  HistoFileSufUp: _JET_JMR__1up
  Symmetrisation: "OneSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

% ------------------------------ %
% -----------   MET  ----------- %
% ------------------------------ %

Systematic: "MET_SoftTrk_ResoPara"
  Title: "MET TST reso. para."
  Type: HISTO
  Category: "E_{T}^{miss} uncertainties"
  HistoFileSufUp: _MET_SoftTrk_ResoPara
  Symmetrisation: "OneSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "MET_SoftTrk_ResoPerp"
  Title: "MET TST reso. perp."
  Type: HISTO
  Category: "E_{T}^{miss} uncertainties"
  HistoFileSufUp: _MET_SoftTrk_ResoPerp
  Symmetrisation: "OneSided"
  Smoothing: 40
  Exclude: QCD,QCD0L

Systematic: "MET_SoftTrk_Scale"
  Title: "MET TST scale"
  Type: HISTO
  Category: "E_{T}^{miss} uncertainties"
  HistoFileSufUp: _MET_SoftTrk_ScaleUp
  HistoFileSufDown: _MET_SoftTrk_ScaleDown
  Symmetrisation: "TwoSided"
  Smoothing: 40
  Exclude: QCD,QCD0L
